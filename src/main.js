import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false
import BootstrapVue from 'bootstrap-vue/dist/bootstrap-vue.esm';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import 'bootstrap/dist/css/bootstrap.css';
import Vuex from 'vuex'
import Store from './vuex/Store'
import VueRouter from "vue-router";
import router from './router/routes.js'
Vue.use(Vuex);
Vue.use(VueRouter);
const store = new Vuex.Store(Store);
Vue.use(BootstrapVue);
require('vue2-animate/dist/vue2-animate.min.css')
new Vue({
  render: h => h(App),
  store,
  router
}).$mount('#app')
