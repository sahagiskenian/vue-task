 
import VueRouter from "vue-router";
import AllRecepies from '../components/Recepies/AllRecepies'
import LoginComponent from '../components/Auth/LoginComponent'
import RegisterComponent from '../components/Auth/RegisterComponent'
const routes = [
    {
        path: "/",
        component: AllRecepies,
        name: "home"
    },
    {
        path: "/login",
        component: LoginComponent,
        name: "login"
    },
    {
        path: "/register",
        component: RegisterComponent,
        name: "register"
    },


];

const router = new VueRouter({
    scrollBehavior (to, from, savedPosition) {
        if (savedPosition) {
          return savedPosition
        } else {
          return { x: 0, y: 0 }
        }
      },

    routes, // short for `routes: routes`
    mode: "history"
});
router.beforeEach((to, from, next) => {

    if(to.matched.some(record => record.meta.requiresAuth)) {
        if (localStorage.getItem('isLoggedIn') == 'false') {
            next({
                path: '/',
                params: { nextUrl: to.fullPath }
            })
        }
            else {
                next()
            }
        }
        else {
            next()
        }

})

export default router;
