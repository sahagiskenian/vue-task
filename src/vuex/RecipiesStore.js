

export default {
    state: {


        recps: [
            { id: 1, name: "first element", description: 'Dickerson', image: 'https://picsum.photos/600/300/?image=25' },
            { id: 2, name: "sec element", description: 'Larsen', image: 'https://picsum.photos/600/300/?image=24' },
            { id: 3, name: "mid element", description: 'Geneva', image: 'https://picsum.photos/600/300/?image=22' },
            { id: 4, name: "after mid element", description: 'Jami', image: 'https://picsum.photos/600/300/?image=21' }
        ],
        editrecepie: {
            id: 0,
            name: "",
            description: "",
            image: "",
        }


    },
    mutations: {

        addToRecp(state, payload) {
            state.recps = [...state.recps, payload];
        },
        removeFromRecps(state, payload) {
            state.recps = state.recps.filter(item => item.id !== payload);
        },
        GetEdit(state, payload) {
            state.editrecepie = payload;
        },
        Edit(state, {objIndex,payload}) {
    
            if (objIndex>-1) {
                state.recps[objIndex].id = payload.id;
                state.recps[objIndex].name = payload.name;
                state.recps[objIndex].description = payload.description;
                state.recps[objIndex].image = payload.image;
            }

        },


    },
    actions: {

        AddRecepie({ commit }, recps) {
            console.log(JSON.stringify(recps))
            commit('addToRecp', recps);
        },
        removeFromRecps({ commit }, id) {

            commit('removeFromRecps', id);




        },

        GetEdit({ commit }, payload) {

         
            commit('GetEdit', payload);




        },
        EditRecepie({ commit,state }, payload) {

        

            commit('Edit', { objIndex: state.recps.findIndex((obj => obj.id == payload.id)),payload});




        },



    },
    getters: {
        recps: (state) => state.recps,
        editrecepie: (state) => state.editrecepie,



    }

};
