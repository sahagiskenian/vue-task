

export default {
    state: {


        user: {

        },
        isLoggedIn: localStorage.getItem('token') ? true : false,



    },
    mutations: {

        LoggedIn(state, payload) {
            localStorage.setItem('token', "token");
            state.isLoggedIn = true;
            state.user = payload;
        },
        Register(state, payload) {
            localStorage.setItem('token', "token");
            state.isLoggedIn = true;
            state.user = payload;
        },
        Logout(state) {
            localStorage.removeItem('token');
            state.isLoggedIn = false;
            state.user = '';
        },


    },
    actions: {

        Login({ commit }, user) {
           // console.log(user);
            if (user.email == "example@example.org" && user.password == "password") {
                commit("LoggedIn", user);
                alert("  Success");
                window.location.replace("/");
            }
            else {
                alert("Wrong Credentials");
             
            }


        },
        Register({ commit }, user) {

            commit("LoggedIn", user)

            alert("  Success");
            window.location.replace("/");
        },
        Logout({ commit }) {

            commit("Logout")

            alert("  Success");

        },



    },
    getters: {
        user: (state) => state.user,
        isLoggedIn:(state)=>state.isLoggedIn



    }

};
